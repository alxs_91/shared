package contracts

import "time"

type EventCreatedEvent struct {
	ID         string    `json:"id"`
	Name       string    `json:"name"`
	LocationID string    `json:"location"`
	Start      time.Time `json:"startTime"`
	End        time.Time `json:"endTime"`
}

func (e *EventCreatedEvent) EventName() string {
	return "event.created"
}
